<?php
namespace Intercom\PartyInvite;

/**
 * Created by PhpStorm.
 * User: Amir Cicak
 * Date: 2017-04-02
 * Time: 14:23
 */
class GeoHelpers
{
    //checks if point is inside rectangle (wanted to use this to filter out points outside of distance bounding box
    //to prevent calling expensive harvesine function for all points but didnt have time to finish it (client code))
    public static function inBounds($pointLat, $pointLong, $boundsNElat, $boundsNElong, $boundsSWlat, $boundsSWlong)
    {
        $eastBound = $pointLong < $boundsNElong;
        $westBound = $pointLong > $boundsSWlong;
        if ($boundsNElong < $boundsSWlong) {
            $inLong = $eastBound || $westBound;
        } else {
            $inLong = $eastBound && $westBound;
        }
        $inLat = $pointLat > $boundsSWlat && $pointLat < $boundsNElat;
        return $inLat && $inLong;
    }

    //computes haversine distance in KM
    public static function haversine($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371)
    {
        // convert from degrees to radians
        $latFrom = deg2rad($latitudeFrom);
        $lonFrom = deg2rad($longitudeFrom);
        $latTo = deg2rad($latitudeTo);
        $lonTo = deg2rad($longitudeTo);
        $latDelta = $latTo - $latFrom;
        $lonDelta = $lonTo - $lonFrom;
        $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) + cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
        return $angle * $earthRadius;
    }
}