<?php
namespace Intercom\PartyInvite;

/**
 * Created by PhpStorm.
 * User: Amir Cicak
 * Date: 2017-04-02
 * Time: 14:33
 */
class Customer implements LocatableInterface
{
    protected $id;
    protected $name;
    protected $location;

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    /**
     * @return Point
     */
    public function getLocation()
    {
        return $this->location;
    }

    public function __construct($id, $name, $location = null)
    {
        $this->id = $id;
        $this->name = $name;
        $this->location = $location;
    }

    public function setLocation(Point $location)
    {
        $this->location = $location;
    }

    public static function getSortFunction($getterToSortBy)
    {
        return function (Customer $a, Customer $b) use ($getterToSortBy){
            if (call_user_func([$a, $getterToSortBy]) == call_user_func([$b, $getterToSortBy])) {
                return 0;
            }
            return (call_user_func([$a, $getterToSortBy]) < call_user_func([$b, $getterToSortBy])) ? -1 : 1;
        };
    }
}