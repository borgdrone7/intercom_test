<?php
/**
 * Created by PhpStorm.
 * User: Amir Cicak
 * Date: 2017-04-02
 * Time: 17:42
 */

namespace Intercom\PartyInvite;


class FilterByDistance
{
    protected $referencePoint;
    protected $maxDistance;

    public function __construct(Point $referencePoint, $maxDistance)
    {
        $this->referencePoint = $referencePoint;
        $this->maxDistance = $maxDistance;
    }

    /**
     * @var $locatables LocatableInterface []
     */
    public function filter($locatables)
    {
        $filtered=[];
        foreach ($locatables as $locatable) {
            $source=$locatable->getLocation();
            $target=$this->referencePoint;
            if(GeoHelpers::haversine($source->getLat(), $source->getLon(), $target->getLat(), $target->getLon())<=$this->maxDistance) {
                $filtered[]=$locatable;
            }
        }
        return $filtered;
    }
}