<?php
/**
 * Created by PhpStorm.
 * User: Amir Cicak
 * Date: 2017-04-02
 * Time: 17:05
 */

namespace Intercom\PartyInvite;

class ValidatedDataReaderDecorator implements DataReaderInterface
{
    protected $validator;
    protected $reader;

    public function __construct(DataReaderInterface $reader, DataValidatorInterface $validator)
    {
        $this->validator=$validator;
        $this->reader=$reader;
    }
    /*
     * @return \Traversable
     */
    public function read()
    {
        $data=$this->reader->read();
        foreach ($data as $datum) {
            $valid=$this->validator->validate($datum);
            if(!$valid->valid) {
                throw new \Exception("Input data not properly formatted. " . implode(",", $valid->errors));
            }
        }
        return $data;
    }
}