<?php
/**
 * Created by PhpStorm.
 * User: Amir Cicak
 * Date: 2017-04-02
 * Time: 17:34
 */

namespace Intercom\PartyInvite;


class CustomerBuilder
{
    protected $adapter;
    protected $transformer;
    protected $reader;

    public function __construct(DataReaderInterface $reader, DataAdapterInterface $adapter, CustomerTransformerInterface $transformer)
    {
        $this->adapter = $adapter;
        $this->transformer = $transformer;
        $this->reader = $reader;
    }

    /**
     * @return Customer []
     */
    public function getCustomers()
    {
        $customers=[];
        $data=$this->reader->read();
        foreach ($data as $datum) {
            $customers []= $this->transformer->getCustomer($this->adapter->convertToArray($datum));
        }
        return $customers;
    }
}