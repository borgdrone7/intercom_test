<?php
/**
 * Created by PhpStorm.
 * User: Amir Cicak
 * Date: 2017-04-02
 * Time: 17:05
 */

namespace Intercom\PartyInvite;


interface DataReaderInterface
{
    public function read();
}