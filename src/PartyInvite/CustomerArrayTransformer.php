<?php
namespace Intercom\PartyInvite;

/**
 * Created by PhpStorm.
 * User: Amir Cicak
 * Date: 2017-04-02
 * Time: 14:33
 */
class CustomerArrayTransformer implements CustomerTransformerInterface
{
    /**
     * @param $inputData
     * @return Customer
     */
    public function getCustomer($inputData)
    {
        $customer=new Customer($inputData["user_id"], $inputData["name"]);
        $customer->setLocation(new Point($inputData["latitude"], $inputData["longitude"]));
        return $customer;
    }
}