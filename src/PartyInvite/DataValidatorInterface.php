<?php
/**
 * Created by PhpStorm.
 * User: Amir Cicak
 * Date: 2017-04-02
 * Time: 18:13
 */

namespace Intercom\PartyInvite;


interface DataValidatorInterface
{
    public function validate($data);
}