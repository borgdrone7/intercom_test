<?php
/**
 * Created by PhpStorm.
 * User: Amir Cicak
 * Date: 2017-04-02
 * Time: 17:05
 */

namespace Intercom\PartyInvite;

//this stream reader reads everything in memory (be aware of that limitation)
class StreamDataReader implements DataReaderInterface
{
    protected $streamName;
    public function __construct($streamName)
    {
        $this->streamName=$streamName;
    }
    /*
     * @return \Traversable
     */
    public function read()
    {
        $output=[];
        try {
            $handle = fopen($this->streamName, "r");
        } catch (\Exception $e) {
            throw new \Exception("Couldn't open data source: {$this->streamName} ({$e->getMessage()})");
        }
        while (($line = fgets($handle)) !== false) {
            $output[]=trim($line);
        }
        fclose($handle);
        return $output;
    }
}