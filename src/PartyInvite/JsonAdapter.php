<?php
/**
 * Created by PhpStorm.
 * User: Amir Cicak
 * Date: 2017-04-02
 * Time: 16:47
 */

namespace Intercom\PartyInvite;


class JsonAdapter implements DataAdapterInterface
{

    public function convertToArray($inputData)
    {
        return json_decode($inputData, true);
    }
}