<?php
namespace Intercom\PartyInvite;

/**
 * Created by PhpStorm.
 * User: Amir Cicak
 * Date: 2017-04-02
 * Time: 14:33
 */
interface CustomerTransformerInterface
{
    /**
     * @param $inputData
     * @return Customer
     */
    public function getCustomer($inputData);
}