<?php
/**
 * Created by PhpStorm.
 * User: Amir Cicak
 * Date: 2017-04-02
 * Time: 17:43
 */

namespace Intercom\PartyInvite;


interface LocatableInterface
{
    /**
     * @return Point
     */
    public function getLocation();
}