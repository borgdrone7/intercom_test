<?php
namespace Intercom\PartyInvite;

/**
 * Created by PhpStorm.
 * User: Amir Cicak
 * Date: 2017-04-02
 * Time: 14:33
 */

class CustomerJsonValidator implements DataValidatorInterface
{
    protected $schema;

    public function __construct($schema)
    {
        $this->schema = $schema;
    }

    public function validate($data)
    {
        $res = \Jsv4::validate((object)json_decode($data), json_decode($this->schema));
        $errs = array_map(function ($x) {
            return $x->message;
        }, $res->errors);
        return (object)["valid" => $res->valid, "errors" => $errs];
    }
}