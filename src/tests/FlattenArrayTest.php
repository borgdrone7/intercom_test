<?php
/**
 * Created by PhpStorm.
 * User: Amir Cicak
 * Date: 2017-03-30
 * Time: 19:25
 */

namespace Intercom\tests;

use Intercom\ArrayFlatten\ArrayFlattenInterface;
use Intercom\ArrayFlatten\NonRecursiveArrayFlattener;
use Intercom\ArrayFlatten\RecursiveArrayFlattener;
use PHPUnit\Framework\TestCase;

class FlattenArrayTest extends TestCase
{
    protected $helper;

    public function __construct()
    {
        parent::__construct();
        $this->helper = new FlattenArrayHelper();
    }

    public function testRecursiveInvalidArgumentException()
    {
        $this->checkInvalidArgumentException(new RecursiveArrayFlattener());
    }

    public function testRecursiveArrayFlattener()
    {
        $this->flattenSamples(new RecursiveArrayFlattener());
    }

    public function testNonRecursiveArrayFlattener()
    {
        $this->flattenSamples(new NonRecursiveArrayFlattener());
    }

    public function testNonRecursiveMaximumNestedLevelOverflow()
    {
        $flattener=new NonRecursiveArrayFlattener();
        $superNestedArray = $this->helper->getVeryDeepArray(300);
        $expectedFlattenedArray = [1];
        $flattenedArray = $flattener->flattenArray($superNestedArray);
        $this->assertSame($expectedFlattenedArray, $flattenedArray);
    }

    //this would raise fatal error which is impossible to catch except with phpunit process isolation or register_shutdown_function
    //as this is out of scope of this task I disabled the test by prefixing it with "no"
    public function noTestRecursiveMaximumNestedLevelOverflow()
    {
        $flattener=new RecursiveArrayFlattener();
        //super deep array, recursion maximum level reached
        $superNestedArray = $this->helper->getVeryDeepArray(300);
        $expectedFlattenedArray = [1];
        $flattenedArray = $flattener->flattenArray($superNestedArray);
        $this->assertSame($expectedFlattenedArray, $flattenedArray);
    }

    protected function flattenSamples(ArrayFlattenInterface $flattener)
    {
        $arraySamples = $this->helper->getSamples();
        foreach ($arraySamples as $arraySample) {
            $flattenedArray = $flattener->flattenArray($arraySample['actual']);
            $this->assertSame($arraySample['expected'], $flattenedArray);
        }
    }

    protected function checkInvalidArgumentException(ArrayFlattenInterface $flattener)
    {
        //passing invalid value
        $basicNestedArray = "This is not an array.";
        $this->expectException(\InvalidArgumentException::class);
        $flattener->flattenArray($basicNestedArray);
    }

}