<?php
/**
 * Created by PhpStorm.
 * User: Amir Cicak
 * Date: 2017-03-30
 * Time: 19:25
 */

namespace Intercom\tests;

class FlattenArrayHelper
{

    public function getSamples()
    {
        return [
            [
                //basic array
                "actual"   => [1, [2, 3], 4],
                "expected" => [1, 2, 3, 4]
            ],
            [
                //empty
                "actual"   => [],
                "expected" => []
            ],
            [
                //empty sub array
                "actual"   => [1, [], 4],
                "expected" => [1, 4]
            ],
            [
                //array with nulls and zeros
                "actual"   => [1, null, 2, [1, null, 0], 0, [2, 100, 0, null], 10],
                "expected" => [1, null, 2, 1, null, 0, 0, 2, 100, 0, null, 10]
            ],
            [
                //mutiple nested
                "actual"   => [1, [2, [3, [4, [5, [6]]]]], 7],
                "expected" => [1, 2, 3, 4, 5, 6, 7]
            ],
            [
                //very deep array, under recursion max level
                "actual"   => $this->getVeryDeepArray(200),
                "expected" => [1]
            ]
        ];
    }

    public function getVeryDeepArray($numLevels)
    {
        $veryDeepArray[0] = 1;
        for ($x = 0; $x < $numLevels; $x++) {
            $veryDeepArray[0] = $veryDeepArray;
        }
        return $veryDeepArray;
    }

}