<?php
/**
 * Created by PhpStorm.
 * User: Amir Cicak
 * Date: 2017-04-02
 * Time: 14:36
 */

namespace Intercom\tests;

use Intercom\PartyInvite\Customer;
use Intercom\PartyInvite\CustomerArrayTransformer;
use Intercom\PartyInvite\JsonAdapter;
use Intercom\PartyInvite\Point;
use PHPUnit\Framework\TestCase;

class PartyStructsTest extends TestCase
{
    protected function getSampleCustomer()
    {
        return new Customer(1, "John Doe", new Point(53.3393, -6.2576841));
    }
    protected function getSampleArray()
    {
        return ["user_id" => 1, "name" => "John Doe", "latitude" => 53.3393, "longitude" => -6.2576841];
    }
    protected function getSampleJsonLine()
    {
        return '{"latitude": "53.3393", "user_id": 1, "name": "John Doe", "longitude": "-6.2576841"}';
    }

    public function testTransformer()
    {
        $this->assertEquals($this->getSampleCustomer(), (new CustomerArrayTransformer())->getCustomer($this->getSampleArray()));
    }
    public function testAdapter()
    {
        $this->assertEquals($this->getSampleArray(), (new JsonAdapter())->convertToArray($this->getSampleJsonLine()));
    }

    public function testCustomer()
    {
        $customer=$this->getSampleCustomer();
        $this->assertSame(1, $customer->getId());
        $this->assertEquals(-6.2576841, $customer->getLocation()->getLon(), "Longitude not ok", 0.000000001);
        $customers=[$this->getSampleArray(), (new JsonAdapter())->convertToArray($this->getSampleJsonLine())];
        for ($i = 0; $i < 2; $i++) {
            $arrayRepo = new CustomerArrayTransformer();
            $customerFromRepo = $arrayRepo->getCustomer($customers[$i]);
            $this->assertEquals($customer, $customerFromRepo);
        }

    }
}