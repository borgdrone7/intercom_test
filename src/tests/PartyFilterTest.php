<?php
/**
 * Created by PhpStorm.
 * User: Amir Cicak
 * Date: 2017-04-02
 * Time: 14:36
 */

namespace Intercom\tests;

use Intercom\PartyInvite\Customer;
use Intercom\PartyInvite\CustomerArrayTransformer;
use Intercom\PartyInvite\CustomerBuilder;
use Intercom\PartyInvite\FilterByDistance;
use Intercom\PartyInvite\JsonAdapter;
use Intercom\PartyInvite\Point;
use Intercom\PartyInvite\StreamDataReader;
use PHPUnit\Framework\TestCase;

class PartyFilterTest extends TestCase
{
    protected function getSampleFile()
    {
        return __DIR__ . "/data/customers.json";
    }

    public function testFilter()
    {
        $builder=new CustomerBuilder(new StreamDataReader($this->getSampleFile()), new JsonAdapter(), new CustomerArrayTransformer());
        $customers=$builder->getCustomers();
        $filter=new FilterByDistance(new Point(53.3393,-6.2576841), 100);
        /** @var Customer [] $filteredCustomers */
        $filteredCustomers=$filter->filter($customers);
        $this->assertEquals(16, count($filteredCustomers));
        usort($filteredCustomers, Customer::getSortFunction("getId"));
        $this->assertEquals("Ian Kehoe", $filteredCustomers[0]->getName());
    }
}