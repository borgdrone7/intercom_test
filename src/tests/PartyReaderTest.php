<?php
/**
 * Created by PhpStorm.
 * User: Amir Cicak
 * Date: 2017-04-02
 * Time: 14:36
 */

namespace Intercom\tests;
use Intercom\PartyInvite\CustomerArrayTransformer;
use Intercom\PartyInvite\CustomerBuilder;
use Intercom\PartyInvite\CustomerJsonValidator;
use Intercom\PartyInvite\JsonAdapter;
use Intercom\PartyInvite\StreamDataReader;
use Intercom\PartyInvite\ValidatedDataReaderDecorator;
use PHPUnit\Framework\TestCase;

class PartyReaderTest extends TestCase
{
    protected function getValidator()
    {
        return new CustomerJsonValidator(file_get_contents($this->getCustomerSchema()));
    }
    protected function getSampleFile()
    {
        return __DIR__ . "/data/customers.json";
    }
    protected function getInvalidSampleFile()
    {
        return __DIR__ . "/data/customers_with_error.json";
    }
    protected function getCustomerSchema()
    {
        return __DIR__ . "/data/customers_schema.json";
    }
    public function testReadFileException()
    {
        $reader=new StreamDataReader("nosuchfile");
        $this->expectException(\Exception::class);
        $reader->read();
    }

    public function testValidatedReader()
    {
        $reader=new ValidatedDataReaderDecorator(new StreamDataReader($this->getSampleFile()), $this->getValidator());
        $data=$reader->read();
        $this->assertSame(32, count($data));

        $reader=new ValidatedDataReaderDecorator(new StreamDataReader($this->getInvalidSampleFile()), $this->getValidator());
        try {
            $reader->read();
        } catch (\Exception $e) {
            $this->assertEquals("Input data not properly formatted. Missing required property: user_id", $e->getMessage());
        }

    }
    public function testReader()
    {
        $reader=new StreamDataReader($this->getSampleFile());
        $data=$reader->read();
        $this->assertSame(32, count($data));
        $adapter=new JsonAdapter();
        $transformer=new CustomerArrayTransformer();
        $customers=[];
        foreach ($data as $datum) {
            $customers []= $transformer->getCustomer($adapter->convertToArray($datum));
        }
        $builder=new CustomerBuilder($reader, $adapter, $transformer);
        $builderCustomers=$builder->getCustomers();
        $this->assertEquals($customers, $builderCustomers);
        $this->assertEquals("Jack Enright", $builderCustomers[3]->getName());
    }

    public function testValidator()
    {
        $reader=new StreamDataReader($this->getSampleFile());
        $data=$reader->read();
        $validator=new CustomerJsonValidator(file_get_contents($this->getCustomerSchema()));
        $valid=$validator->validate($data[0]);
        $this->assertTrue($valid->valid);

        $valid=$validator->validate('{"latitude1": "53.3393", "user_id": "ds", "longitude": "-6.2576841"}');
        $this->assertFalse($valid->valid);
        $this->assertEquals(3, count($valid->errors));
        $this->assertEquals("Missing required property: name,Missing required property: latitude,Invalid type: string", implode(",", $valid->errors));
    }
}