<?php
namespace Intercom\ArrayFlatten;

/**
 * Created by PhpStorm.
 * User: Amir Cicak
 * Date: 2017-03-30
 * Time: 19:18
 */
class NonRecursiveArrayFlattener implements ArrayFlattenInterface
{
    public function flattenArray($nestedArray)
    {
        if(!is_array($nestedArray)) {
            throw new \InvalidArgumentException("flattenArray method accepts only array");
        }
        $stack=[];
        $flattenedArray=[];
        while(true) {
            if (count($nestedArray)>0) {
                $elem=array_shift($nestedArray);
                if(is_array($elem)) {
                    array_push($stack, $nestedArray);
                    $nestedArray=$elem;
                } else {
                    $flattenedArray[]=$elem;
                }
            } else {
                if(count($stack)>0) {
                    $nestedArray=array_pop($stack);
                } else {
                    break;
                }
            }
        }
        return $flattenedArray;
    }
}