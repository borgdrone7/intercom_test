<?php
namespace Intercom\ArrayFlatten;

/**
 * Created by PhpStorm.
 * User: Amir Cicak
 * Date: 2017-03-30
 * Time: 19:18
 */
class RecursiveArrayFlattener implements ArrayFlattenInterface
{
    public function flattenArray($nestedArray)
    {
        if(!is_array($nestedArray)) {
            throw new \InvalidArgumentException("flattenArray method accepts only array");
        }
        $flattenedArray=[];
        foreach ($nestedArray as $arrayElement) {
            if(is_array($arrayElement)) {
                $flattenedSubArray=$this->flattenArray($arrayElement);
                $flattenedArray = array_merge( $flattenedArray, $flattenedSubArray );
            } else {
                $flattenedArray [] = $arrayElement;
            }
        }
        return $flattenedArray;
    }
}