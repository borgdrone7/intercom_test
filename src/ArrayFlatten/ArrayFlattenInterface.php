<?php
/**
 * Created by PhpStorm.
 * User: Amir Cicak
 * Date: 2017-04-02
 * Time: 00:02
 */

namespace Intercom\ArrayFlatten;


interface ArrayFlattenInterface
{
    public function flattenArray($nestedArray);
}