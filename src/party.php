<?php
/**
 * Created by PhpStorm.
 * User: Amir Cicak
 */
use Intercom\PartyInvite\Customer;
use Intercom\PartyInvite\CustomerArrayTransformer;
use Intercom\PartyInvite\CustomerBuilder;
use Intercom\PartyInvite\CustomerJsonValidator;
use Intercom\PartyInvite\FilterByDistance;
use Intercom\PartyInvite\JsonAdapter;
use Intercom\PartyInvite\Point;
use Intercom\PartyInvite\StreamDataReader;
use Intercom\PartyInvite\ValidatedDataReaderDecorator;
require_once __DIR__ . '/../vendor/autoload.php';

//PLEASE NOTE, THIS CODE IS RAW, POINT IS JUST TO SHOW USAGE OF ANOTHER CODE BUT IT IS NON POLISHED
//BASICALLY IT IS COPY PASTE FROM TESTS AND JUST MODIFIED TO WORK FROM COMAND LINE
global $argv;
if ((count($argv) != 2) || (!empty($argv[1]) && !is_numeric($argv[1]))) {
    echo "Usage: party.php distance. Example: party.php 100" . PHP_EOL;
    exit(1);
}
$distance = $argv[1];
$validator = new CustomerJsonValidator(file_get_contents(__DIR__ . "/tests/data/customers_schema.json"));
$builder = new CustomerBuilder(
    new ValidatedDataReaderDecorator(new StreamDataReader("php://stdin"), $validator),
    new JsonAdapter(),
    new CustomerArrayTransformer()
);
try {
    $customers = $builder->getCustomers();
} catch (\Exception $e) {
    echo PHP_EOL. "Please check data: ".$e->getMessage() . PHP_EOL . PHP_EOL;
    exit(0);
}
$office=new Point(53.3393, -6.2576841);
$filter = new FilterByDistance($office, $distance);
/** @var Customer [] $filteredCustomers */
$filteredCustomers = $filter->filter($customers);
usort($filteredCustomers, Customer::getSortFunction("getId"));
printf("%s    %s    %s" . PHP_EOL . PHP_EOL, "num", "id", "name");
$templateMarkers="";
array_map(function (Customer $customer) use (&$templateMarkers) {
    static $x = 1;
    printf("% 3d.  % 3d    %s" . PHP_EOL, $x++, $customer->getId(), $customer->getName());
    $templateMarkers.=sprintf("{position: {lat: %s, lng: %s}, name: \"%s\"}," . PHP_EOL, $customer->getLocation()->getLat(), $customer->getLocation()->getLon(), $customer->getName());
}, $filteredCustomers);
echo PHP_EOL;

//generate html
$template=file_get_contents(__DIR__."/party.template");
$html=str_replace("<template-markers>", $templateMarkers, $template);
$html=str_replace("<template-office>", sprintf("{lat: %s, lng: %s}", $office->getLat(), $office->getLon()), $html);
$html=str_replace("<template-distance>", $distance * 1000, $html);
file_put_contents(__DIR__ . "/party.html", $html);