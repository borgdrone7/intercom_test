Hi,

These are comments on tasks given (proudest achievement (1) is actual solution):

1. My current company is doing delivery software for cargo companies. We also do route optimization as part of platform
software we offer. When I first came to the company they used ESRI API for route optimization. It was hardcoded in platform
and also all parameters were hardcoded. It was working but everyone afraid to touch it because it was pretty complex.
I decided to make route optimization a service which would be called from platform. Platform queued requests using IronMQ
and Eta Manager (that's how I called the service) then processed the queue. Now it was possible to install unlimited
Eta Managers on additional machines and get parallel processing of requests and thus scale horizontally. Also, I added support
for different engines. So now we had ESRI, Optrak, Ortec, and our custom route optimizer all configurable dynamically.
Also, since our front end designer was too busy at that time I made front end gui on Platform side in Angular which enabled
Platform admins to select engine and configure parameters per customer and type of delivery. Once configured, platform
would send correct engine configuration to Eta manager. Soon admins started to experiment with it and they figured out
that previous route optimizer we used was less efficient than one from the other company. Also they managed to tweak it
for our purposes by trying different configuration options. As a result now KPI were much better with average route
distance cut of over 3% compared to hardcoded route optimizer we used before. Also, I added retry feature as well as
request, response and error logging to database which helped a lot admins when tweaing the params.
Other (hobby) project I am proud of is 3D engine I was working on from scratch for 3D Arkanoid clone I was doing with my
friends. It is C++ project and I did 3D collision detection and physics for it from scratch. Also I made 3D editor which
supported unlimited undo operations, positioning of 3D objects, assigning them trajectories, particle systems, etc.
 
2. I made both recursive and non recursive version as recursive one has limitation of maximum around 250 nested levels 
(because of stack overflow/maximum nested level function call limitation in php). I simulated real stack using heap stack 
(used php array to simulate it), which removed such limitation (test is provided for that).

3. As I explained I am leaving current company and considering many other possibilities. Needles to say
it takes a lot of time and tests to do. It's late here and I did not manage to polish the code, however I think you can
get basic idea. I wanted to do one refactor too, but if I did every test so detailed I would be losing weeks, not days
doing tests. I wanted to make the code at least near what would be production code which is expected to grow/be changed a lot.
Isn't every code like that? Anyway, I hope it is not too horizontal. I could make more helper classes to make everything
easier to use by client code, but that would probably be done in some kind of IOC depending on framework used. 
I wanted to add Output Transformer support too, but here is 2:00 am, so I just did it raw, same as most of the code in party.php. 
I made it (party.php) as console script which accepts piped data and one argument which is distance from the Office. 
Also, party.php generates party.html which represents circle and the points on google maps. Please note I just did this
html generation very raw basically just typed it without any planning, point is to see that locations are indeed within circle.
(please note that pipe code is not polished, for example no input to stdin makes it hang and wait for ctrl+c)
Tests are green:
PHPUnit 6.0.12 by Sebastian Bergmann and contributors.
Time: 152 ms, Memory: 4.00MB
OK (13 tests, 33 assertions)

Examples below.

Output example 1:

>cat tests/data/customers.json | php party.php 100

    num    id    name
    
      1.    4    Ian Kehoe
      2.    5    Nora Dempsey
      3.    6    Theresa Enright
      4.    8    Eoin Ahearn
      5.   11    Richard Finnegan
      6.   12    Christina McArdle
      7.   13    Olive Ahearn
      8.   15    Michael Ahearn
      9.   17    Patricia Cahill
     10.   23    Eoin Gallagher
     11.   24    Rose Enright
     12.   26    Stephen McArdle
     13.   29    Oliver Ahearn
     14.   30    Nick Enright
     15.   31    Alan Behan
     16.   39    Lisa Ahearn

Output Example 2:

>cat tests/data/customers_with_error.json | php party.php 100

    Please check data: Input data not properly formatted. Missing required property: user_id



The Test (what was required to do)

1. What's your proudest achievement? It can be a personal project or something you've worked on professionally. 
Just a short paragraph is fine, but I'd love to know why you're proud of it, 
what impact it had (If any) and any insights you took from it.

2. Write a function that will flatten an array of arbitrarily nested arrays of integers into a flat array of integers. 
e.g. [[1,2,[3]],4] → [1,2,3,4]. If the language you're using has a function to flatten arrays, 
you should pretend it doesn't exist.

3. We have some customer records in a text file (customers.json) -- one customer per line, JSON-encoded. 
We want to invite any customer within 100km of our Dublin office for some food and drinks on us. 
Write a program that will read the full list of customers and output the names and user ids of 
matching customers (within 100km), sorted by User ID (ascending).
o You can use the first formula from this Wikipedia article to calculate distance. 
Don't forget, you'll need to convert degrees to radians.
o The GPS coordinates for our Dublin office are 53.3393,-6.2576841.
o You can find the Customer list here.
⭑ Please don’t forget, your code should be production ready, clean and tested!
 
Important: Please submit your test via the link at the bottom of this email, there is no need to reply directly to this email. 
Once your answers have been submitted, the link will become deactivated and you 
will be taken to a new landing page thanking you for submitting your test!

